<?php
session_start();

if(!array_key_exists("admin", $_SESSION)){
	header("location: login.php");
}


require_once __DIR__."/vendor/autoload.php";
require_once __DIR__."/vharabar/Galery.php";

require_once __DIR__."/scripts/local.php";


$templates = new League\Plates\Engine(__DIR__.'/templates/');

$g = new Galery();

$dir = $g->getAllGaleries('',$_SESSION['admin']);
$list = array();

	
$templates->addData([
	'gals' => $dir,
	'var' => getNavText()
]);

echo $templates->render("allGaleries");


?>