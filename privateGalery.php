<?php
session_start();

if(!array_key_exists("user", $_SESSION)){
	header("location: login.php");
}

require_once __DIR__."/vendor/autoload.php";
require_once __DIR__."/vharabar/Galery.php";

require_once __DIR__."/scripts/local.php";


$templates = new League\Plates\Engine(__DIR__.'/templates/');

$gal = ($_GET['f']);

$g = new Galery();
$gals = $g -> getUsersGaleries($_SESSION['user']['id']);
$gname = "";

$match = '';

// preg_match("/^.+\.(.+$)/", $gals[$gal],$match);
// $gname = $match[1];
$gname = $gals[$gal][1];

$list = $g->getGalleryPhotoList($_SESSION['user']['id'],$gal);

$templates->addData([
	'gal' => $gal,
	'name' => $gname,
	'list' => $list,
	'var' => getNavText()	
	]);

echo $templates->render("privateGalery");


?>