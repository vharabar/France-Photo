<?php
session_start();

require_once __DIR__."/vendor/autoload.php";
require_once __DIR__."/vharabar/Galery.php";

require_once __DIR__."/scripts/local.php";


$templates = new League\Plates\Engine(__DIR__.'/templates/');

$g = new Galery();

$gal = ($_GET['f']);
$list = $g->getPublicGaleryPhotoList($gal);

preg_match("/.*\.(.+)/",$gal,$match);
$gal=$match[1];

$templates->addData([
	'gal' => $gal,
	'list' => $list,
	'var' => getNavText()
	]);
echo $templates->render("publicGalery");


?>