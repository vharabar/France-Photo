<?php

function getNavText(){
		$res['button'] = "Connexion";
		$res['buttonhref'] = "login";
		
	
	if(array_key_exists('user',$_SESSION)){
		$res['button'] = "Déconnexion";
		$res['buttonhref'] = "logout";
	}
		
	$res['cart'] = 0;
	if(array_key_exists('cart',$_SESSION)){
		$res['cart'] = count($_SESSION['cart']);
	}

return $res;
}

?>