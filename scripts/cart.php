<?php
session_start();

require __DIR__."/../vharabar/DB.php";
require __DIR__."/../vharabar/Cart.php";
require __DIR__."/../vharabar/User.php";
require __DIR__."/../vharabar/Input.php";

$in = new Input();
$user = new User();


if(!array_key_exists('user',$_SESSION)){
	$in->sendJson(null);
	return;
}

$c = new Cart();

$data = $in->getJson();
$action = $data['action'];

/* ic for when user whats to get number of distinct photos in cart
$data = ['action' => 'ic'];
*/

$res = strcmp($action, "ic");
if( $res == 0)
	if(array_key_exists('cart',$_SESSION)){
		$in->sendJson($c->getCartCount());
		return;
	}
	else {
		$in->sendJson(0);
		return;
	}

/* ai for when user wants to add an item too the cart
$data = array
		  |
		action => "ai"
  		  |
		items => array
				  |
			   arrays [photoName, format, count]
*/
if( strcmp($data['action'], "ai") == 0){
	$count = 0;
	foreach ($data['items'] as $value)
		if($value[2] != 0)
		{
			# $value = [photoName, fomat, value]
			$c->addItem($value[0],$value[1],$value[2]);
			$count ++;
		}
	$in->sendJson($count);
	return;
}

/* ri for when user want to remove an item from the cart
$data = array 
		  |
		action => ri 
		  |
		item => array 
				 |
			 photoName
			     |
			   format

*/

if( strcmp($data['action'], "ri") == 0){
$c->removeItem($data['item'][0], $data['item'][1]);
$in->sendJson(1);
	return;
}


/* ui for when user want to update item count
$data = array 
		  |
		action => ui 
		  |
		item => array 
				 |
			 photoName
			     |
			   format
			     |
			   count

*/

if( strcmp($data['action'], "ui") == 0){
$r = $c->updateItem($data['item'][0], $data['item'][1], $data['item'][2]);
$in->sendJson($r);
return;
}



?>