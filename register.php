<?php
session_start();

require_once __DIR__."/vendor/autoload.php";
require_once __DIR__."/scripts/local.php";

$templates = new League\Plates\Engine(__DIR__.'/templates/');

$templates->addData([
	'var' => getNavText()
	]);
echo $templates->render("register");


?>