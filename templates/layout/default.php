<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->insert("partials/head") ?>

</head>

<body data-spy="scroll" data-target=".navbar" data-offset="50">
	<div id='home' class="spacer"></div>

	<?php $this->insert("partials/navbar") ?>
	
	<?=$this->section('content')?>
	
	
	<?php $this->insert("partials/footer"); ?>
</body>

</html>