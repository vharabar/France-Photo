<?php $this->layout('layout/default'); ?>

<h1>&nbsp;</h1>
<h1 class="text-center" id="impression">Changement des données personnelles</h1>
<h1>&nbsp;</h1>

<div class="container-fluid" ng-module="fp" ng-controller="update">
	<div class="col-md-4"></div>

	<div class="col-md-4 text-center" id="display">

		<form name="inpt" ng-submit="submit()">

			<div id="pass" class="form-group has-feedback">
				<label for="pwd">Ancien mot de passe:</label>
				<input required id="pass" type="password" class="form-control" name="form.pass" ng-model="form.pass">
				<p>Ceci est nécessaire pour faire un changement des données personnelles</p>
			</div>
			
			<br>

			<div class="form-group has-feedback">
				<label id="email" for="usr">Nouveau Email:</label>
				<input  class="form-control" type="email" name="form.email" ng-model="form.email">
			</div>

			<div class="form-group has-feedback">
				<label for="name">Nouveau Nom:</label>
				<input id="pass" type="text" class="form-control" name="form.name" ng-model="form.name" >
			</div>

			<div class="form-group" >
				<label for="new_pass">Nouveau mot de passe: </label>
				<input type="password" id="new_pass"  class="form-control" name="form.new_pass" ng-model="form.new_pass">
			</div>
			
			<div class="form-group" >
				<label for="new_pass_ver">Vérification mot de passe: </label>
				<input id="pver" type="password" id="new_pass_ver"  class="form-control" name="form.new_pass_ver" ng-model="form.new_pass_ver"
					data-toggle="popover" title="les mots de passe ne coïncident pas">
			</div>

			<button type="submit" class="btn btn-default">Mettre à jour</button>
			<div>&nbsp;</div>

		</form>

	</div>


	<div class="col-md-4"></div>
</div>
<script>
	var con = $("#display");
		var app = angular.module('fp', []);
		app.controller('update', function($scope, $window , $http, $location) {
			$scope.submit = function(){
				$data = $scope.form;
				$data.action='update';
				
				if($data.new_pass.length > 0 && $data.new_pass_ver.length > 0)
				if($data.new_pass != $data.new_pass_ver){
					$('[data-toggle="popover"]').popover("show"); 
					return 0;
				}
				$('[data-toggle="popover"]').popover("hide");
				
				// $window.location.href = "scripts/user.php";
				$http.post("scripts/user.php", JSON.stringify($data)).success(
					function(data,status){
						if(data.code<0){
							con.append( '<div class="alert alert-warning"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Attention!</strong> ' + data.message + ".	</div>");
						}else{
							con.append( '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Succes!</strong> ' + data.message + ".	</div>");
							setTimeout( function () { $window.location.href = "/index.php"},3000);
						}
					});
				return 0;
		}});
</script>