<?php $this->layout('layout/default'); ?>

<h1 class="text-center" id="impression">&nbsp;</h1>
<h1 class="text-center" id="impression">Espace client</h1>
<div class="container-fluid" ng-module="admin">

	<div class="row">
		<div class="col-md-3"></div>

		<div class="col-md-6 text-center">
		</div>
		<div class="col-md-3"></div>
	</div>

	<div class="row">
		<div class="col-md-4"></div>

		<div class="col-md-4 text-center">
			<a href="galeryp.php">
				<div class="well">Accéder à mes galeries photo</div>
			</a>
			<a href="addGal.php">
				<div class="well">Accéder à mes nouvelles photos</div>
			</a>
			<a href="dataChange.php">
				<div class="well">Changer mes données personnels</div>
			</a>
			
			<a href="renameGal.php">
				<div class="well">Renommer les galeries</div>
			</a>

			<?php if(array_key_exists('admin',$_SESSION)) :   ?>
			<a href="commandes.php">
				<div class="well">Voir les commandes</div>
			</a>
			
			<a href="allGaleries.php">
				<div class="well">La liste des galeries</div>
			</a>
			
			<a href="#">
				<div class="well">Mise a jour <br> <h4 onclick="(function() {$('#danger').toggle()})()" >Attention! Site pourra être bloque pour quelques minutes </h4>
				<div class="well" id="danger" style="display: none;">
					<a ng-controller="update" ng-click="update()" href="#">Êtes-vous sûr ? <?=$count?> photos a convertir. </a>
				</div>
				</div>
			</a>
			
			<?php endif ;?>
		</div>

		<div class="col-md-4"></div>
	</div>
</div>

<script>
	var app = angular.module("admin",[]);
	app.controller('update', function ($scope, $http, $window){
		$scope.update = function() {
			console.log('scripts/update.php');
			alert("mise a jour a ete commance");
			$http.get('scripts/update.php');
		}
	});
	
	
</script>