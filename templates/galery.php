<?php $this->layout('layout/default'); ?>

	<h1 class="text-center" id="impression">Galerie</h1>

	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3"></div>

			<div class="col-md-6 text-center">

				<?php foreach($gals as $gal => $info) :?>
				<a href="publicGalery.php?f=<?=$info[0]?>" class="thumbnail">
					<h3 class="text-capitalize"><?=$gal?></h3>
					<img src="getPhoto.php?p=<?=$info[1]?>" style="width:300px;">
				</a>
				<?php endforeach ?>

			</div>

			<div class="col-md-3"></div>
		</div>
	</div>

