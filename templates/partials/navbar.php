<nav class="navbar navbar-invert navbar-fixed-top">
	<div class="container-fluid">
		<div class="navbar-header" style="display: inline;">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">					
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/index.php"><img src="images/Logo.svg" height="50" style="margin-top: -15px;"></a>
		</div>
		<div class="collapse navbar-collapse" id="myNavbar">
			<ul class="nav navbar-nav">

				<li><a href="index.php">Accueil</a></li>
				<li><a href="galery.php">Galerie</a></li>
				<?php if(isset($links)) foreach ($links as $key => $value) : ?>
				<li>
					<a class="scroller" href="<?=$key ?>">
						<span class="scroller-bg">
								<?=$value ?>
								</span>
					</a>
				</li>
				<?php endforeach ;?>

			</ul>
			<ul class="nav navbar-nav navbar-right">
				<?php if(isset($_SESSION['user'])): ?>
				<li><a href="cart.php"><span class="glyphicon glyphicon-shopping-cart"></span> Panier <span ng-module="cartbadge" ng-controller="itemcount" class="badge">{{cart}}</span></a></li>
				<li><a href="client.php"><span class="glyphicon glyphicon-user"></span> Espace client</a></li>
				<?php endif ?>
				<li><a href="<?=$var['buttonhref']?>.php"><span class="glyphicon glyphicon-log-in"></span> <?=$var['button']?></a></li>
			</ul>
		</div>
	</div>
</nav>

<script>
	var update;
	
	var navbar = angular.module('cartbadge', []);
	navbar.controller('itemcount', function($scope, $window, $http, $location, $element) {
		$scope.cart = 0;

		update  = function() {
			$data = {
				"action": "ic"
			};
			$http.post("scripts/cart.php", JSON.stringify($data)).success(
				function(data, status) {
				$scope.cart = data;
				// console.log(data);
				});
		};
		update();
	});

</script>