<?php $this->layout('layout/default'); ?>


	<h1 class="text-center" id="impression">Galerie</h1>

	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3"></div>

			<div class="col-md-6 text-center">

				<?php foreach($gals as $gal => $p) :?>
				<a href="privateGalery.php?f=<?=$gal?>" class="thumbnail">
					<h3 class="text-capitalize"><?=$p[0]?></h3>
					<img src="getPhoto.php?p=<?=$p[1]?>" style="width:300px;">
				</a>
				<?php endforeach ?>

			</div>

			<div class="col-md-3"></div>
		</div>
	</div>

