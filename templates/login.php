<?php $this->layout('layout/default'); ?>

	<h1>&nbsp;</h1>
	<h1 class="text-center" id="impression">Connexion</h1>
	<h1>&nbsp;</h1>

	<div class="container-fluid" ng-app="fp" ng-controller="login">
		<div class="col-md-4"></div>

		<div class="col-md-4 text-center" id="display">
			<form name="inpt" ng-submit="submit()">
				<div class="form-group has-feedback">
					<label id="e" for="usr">Email:</label>
					<input required class="form-control" type="email" name="form.e" ng-model="form.e">
				</div>
				<div id="p" class="form-group has-feedback">
					<label for="pwd">Mot de passe:</label>
					<input required id="pass" type="password" class="form-control" name="form.p" ng-model="form.p" data-toggle="popover" data-trigger="manual"
						title="Mot de passe trop court">
				</div>
				<div class="checkbox">
					<label><input type="checkbox" name="form.r" ng-model="form.r" value="">Se souvenir de moi</label>
				</div>
				<button type="submit" class="btn btn-default">Connexion</button>
				<div>&nbsp;</div>
			</form>
			<h2 class="text-center">Êtes-vous client ?<br><a href='register.php'>Enregistrez-vous !</a> </h2>
		</div>	
		

		<div class="col-md-4"></div>
	</div>
	<script>
		var con = $("#display");
		var app = angular.module('fp', []);
		app.controller('login', function($scope, $window , $http, $location) {

			$scope.submit = function(){
				$data = $scope.form;
				$data.action='login';
				
				
				if($data.p.length < 6){
					$("[data-toggle='popover'][name='form.p']").popover('show');
					
					return false;
				}
				
				$("[data-toggle='popover'][name='form.p']").popover('hide');

				// $window.location.href = "scripts/user.php";
				$http.post("scripts/user.php", JSON.stringify($data)).success(
					function(data,status){
						if(data.code<0){
							con.append( '<div class="alert alert-warning"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Attention!</strong> ' + data.message + ".	</div>");
						}else{
							con.append( '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Succes!</strong> ' + data.message + ".	</div>");
							setTimeout( function () { $window.location.href = "/index.php"},3000);
						}
					});
				}
			});
	</script>
