<?php $this->layout('layout/default'); ?>


<div class="container-fuid" style="background-color: rgb(80.4%, 96%, 48.7%);">
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<div id="imgserv" class="carousel slide text-center" data-ride="carousel" data-interval="7000">
				<!-- Indicators -->
				<ol class="carousel-indicators">
					<li data-target="#imgserv" data-slide-to="0" class="active"></li>
					<li data-target="#imgserv" data-slide-to="1"></li>
					<li data-target="#imgserv" data-slide-to="2"></li>
					<li data-target="#imgserv" data-slide-to="3"></li>
					<li data-target="#imgserv" data-slide-to="4"></li>
					<li data-target="#imgserv" data-slide-to="5"></li>
				</ol>

				<!-- Wrapper for slides -->
				<div class="carousel-inner" role="listbox">
					<div class="item active">
						<a href="#">
							<img class="carousel-img carousel-img-center " src='images/DSC_4679.jpg'>
							
							<div class="carousel-caption text-adjust">
								<p>
									France-Photo depuis 30 ans à votre service !
									<br>
									<br> Nous vous accueillons au cœur de Clisson pour tout projet lié à la photo : mariage, portrait, photo d’industrie,
									tirages de vos photos numériques de toutes dimensions, reproduction de photos, encadrement, transfert de film sur
									dvd…
									<br>
									<br> Vous avez un projet, mais vous êtes trop pris, nous venons chez vous pour écouter vos attentes et vous établir
									un devis

								</p>
							</div>
						</a>
					</div>
					<div class="item">
						<a href="publicGalery.php?f=1.Mariage">
							<img class="carousel-img carousel-img-center " src='images/DSC_3947.jpg'>
						</a>
					</div>
					<div class="item">
						<a href="publicGalery.php?f=8.Cabine">
							<img class="carousel-img carousel-img-center " src='images/image cabine.jpg'>
						</a>
					</div>
					<div class="item">
						<a href="publicGalery.php?f=4.Portrait">
							<img class="carousel-img carousel-img-center " src='images/portrait naissance lien vers gallerie portrait.jpg'>
						</a>
					</div>
					<div class="item">
							<img class="carousel-img carousel-img-center " src='images/faq_19_photo_grande_1452358732.jpg'>
						<div class="carousel-caption">
							<dl>
								<dt>ENCADREMENT SUR MESURE          </dt>
								<dd>Cadre complet</dd>
								<dd>Cadre vide</dd>
								<dd>Cadre pour toile ou canevas</dd>
								<dd>Caisse américaine</dd>
								<dd>Création de Passe-Partouts</dd>
								<dd>Montage bois</dd>
								<dd>Plexiglass</dd>
								<dd>Vos photos sur toile</dd>
							</dl>
						</div>
					</div>
					<div class="item">
							<img class="carousel-img carousel-img-center " src='images/Plaquette_10x15 copie.jpg'>
					</div>
				</div>

				<!-- Left and right controls -->
				<a class="left carousel-control" href="#imgserv" role="button" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="right carousel-control" href="#imgserv" role="button" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>
		</div>
		<div class="col-md-2"></div>
	</div>
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8 well villecontainer">
			<h4 class="text-center villes"> Clisson | Montaigu | Vallet | Cholet | Les Herbiers </h4>
		</div>
		<div class="col-md-2"></div>
	</div>
</div>
<h1>&nbsp;</h1>
<h1>&nbsp;</h1>
<h1 class="text-center" id="impression">Impression rapide</h1>
<h1>&nbsp;</h1>

<div class="contaier-fluid" style="background-color: rgb(100%, 74.9%, 44.5%);">
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-2">
			<div id="imprimantes" class="carousel slide text-center" data-ride="carousel" data-interval="5000">
				<!-- Indicators -->
				<ol class="carousel-indicators">
					<li data-target="#imprimantes" data-slide-to="0" class="active"></li>
					<li data-target="#imprimantes" data-slide-to="1"></li>
				</ol>

				<!-- Wrapper for slides -->
				<div class="carousel-inner text-center" role="listbox">
					<div class="item active">
							<img class="carousel-img " src='../images/impr1.jpg'>
				</div>
				<div class="item">
							<img class="carousel-img " src='../images/impr2.jpg'>
			</div>
		</div>

		<!-- Left and right controls -->
		<a class="left carousel-control" href="#imprimantes" role="button" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="right carousel-control" href="#imprimantes" role="button" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>

</div>
<div class="col-md-6 text-center">
	<div class="well well-large">
		<h2>
Nous vous proposons une gamme de tirage complète, <br>
rapide et de haute qualité afin de satisfaire<br>
vos exigences du 10X15 au format 60X80<br><br>
– Borne numérique pour un tirage immédiat <br>
– Laboratoire argentique pour une qualité optimum

</h2>
	</div>
</div>
<div class="col-md-2"></div>

</div>
</div>



<h1>&nbsp;</h1>
<h1>&nbsp;</h1>
<h1 class="text-center" id="cabine">Location cabine</h1>
<h1>&nbsp;</h1>

<div class="container-fluid text-center" style="background-color: rgb(26.9%, 36.8%, 98.7%);">
	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-5">
			<div class="well well-large" style="color: #ddd">
				<h2>
Louez une cabine photo <br>
avec tirages instantanés pour vos fêtes,<br>
mariage, anniversaires, seminaires....
</h2>
			</div>
		</div>
		<div class="col-md-5">
			<div id="imgcabine" class="carousel slide text-center" data-ride="carousel" data-interval="5000">
				<!-- Indicators -->
				<ol class="carousel-indicators">
					<li data-target="#imgcabine" data-slide-to="0" class="active"></li>
					<li data-target="#imgcabine" data-slide-to="1"></li>
					<li data-target="#imgcabine" data-slide-to="2"></li>
					<li data-target="#imgcabine" data-slide-to="3"></li>
					<li data-target="#imgcabine" data-slide-to="4"></li>
					<li data-target="#imgcabine" data-slide-to="5"></li>
					<li data-target="#imgcabine" data-slide-to="6"></li>
					<li data-target="#imgcabine" data-slide-to="7"></li>
				</ol>

				<!-- Wrapper for slides -->
				<div class="carousel-inner" role="listbox">

					<div class="item active">
							<img class="carousel-img " src='getPhoto.php?p=p.8.Cabine.20160108_200455'>
					</div>
					<div class="item">
							<img class="carousel-img " src='getPhoto.php?p=p.8.Cabine.20160108_200504'>
					</div>
					<div class="item">
							<img class="carousel-img " src='getPhoto.php?p=p.8.Cabine.20160108_202828'>
					</div>
					<div class="item">
							<img class="carousel-img " src='getPhoto.php?p=p.8.Cabine.20160108_202845'>
					</div>
					<div class="item">
							<img class="carousel-img " src='getPhoto.php?p=p.8.Cabine.20160108_203145'>
					</div>
					<div class="item">
							<img class="carousel-img " src='getPhoto.php?p=p.8.Cabine.20160108_203225'>
					</div>
					<div class="item">
							<img class="carousel-img " src='getPhoto.php?p=p.8.Cabine.20160108_203942'>
					</div>
					<div class="item">
							<img class="carousel-img " src='getPhoto.php?p=p.8.Cabine.20160108_205006'>
					</div>
				</div>

				<!-- Left and right controls -->
				<a class="left carousel-control" href="#imgcabine" role="button" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="right carousel-control" href="#imgcabine" role="button" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>
		</div>
		<div class="col-md-1"></div>
	</div>
</div>

<h1>&nbsp;</h1>
<h1>&nbsp;</h1>
<h1 class="text-center" id="partners">Partenaires</h1>
<h1>&nbsp;</h1>

<!-- LES LOGOS DES PARTENAIRS -->
<div class="container-fluid text-center">
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<a href="http://www.le-baobab.net/" class="thumbnail">
				<img src='images/banner1.jpg' class="img-responsive">
			</a>
			<a href="http://www.creations-deco.com/" class="thumbnail">
				<img src='images/IMG_6197.JPG' class="img-responsive">
			</a>
		</div>
		<div class="col-md-2"></div>

	</div>
</div>

<h1>&nbsp;</h1>
<h1>&nbsp;</h1>
<h1 class="text-center" id="contacts">Contacts</h1>
<h1>&nbsp;</h1>

<div class="container-fluid text-left">
	<div class="row" style="height:800px;">
		<div class="col-md-2"></div>
		<div class="col-md-4" style="height:100%;">
			<div class="well" style="height:100%;">
				<h3>Pascal VINET</h3>
				<h3>Alexandre CONSTANTY</h3>
				<br>
				<h3><span class="glyphicon glyphicon-envelope"></span> Email </h3>
				<h4>
				<span class="glyphicon glyphicon-earphone" style="color: transparent;"></span>
				<span class="glyphicon glyphicon-earphone" style="color: transparent;"></span><a href="mailto:francephoto44@gmail.com" title="France Photo">francephoto44@gmail.com</a>
				</h4>
				<h3><span class="glyphicon glyphicon-map-marker"></span> Adresse </h3>
				<h4>
				<span class="glyphicon glyphicon-earphone" style="color: transparent;"></span>
				<span class="glyphicon glyphicon-earphone" style="color: transparent;"></span>France-Photo<br>
				<span class="glyphicon glyphicon-earphone" style="color: transparent;"></span>
				<span class="glyphicon glyphicon-earphone" style="color: transparent;"></span>11, place st Jacques<br>
				<span class="glyphicon glyphicon-earphone" style="color: transparent;"></span>
				<span class="glyphicon glyphicon-earphone" style="color: transparent;"></span>44190 Clisson
				</h4>
				<h3><span class="glyphicon glyphicon-earphone"></span> Telephone </h3>
				<h4><span class="glyphicon glyphicon-earphone" style="color: transparent;">
					</span><span class="glyphicon glyphicon-earphone" style="color: transparent;"></span>
					02-40-54-33-43 <br>
					<span class="glyphicon glyphicon-earphone" style="color: transparent;">
					</span><span class="glyphicon glyphicon-earphone" style="color: transparent;"></span>
					06-68-17-50-01
				</h4>
				<h3><span class="glyphicon glyphicon-time"></span> Ouverture:</h3>
				<table class="table table-striped">
					<tr>
						<th>
							Jour
						</th>
						<th>
							Horaire
						</th>
					</tr>
					<tr>
						<td>Lundi</td>
						<td>Ferme </td>
					</tr>
					<tr>
						<td>Mardi</td>
						<td>9h45 - 12h15 14h30 - 19h00</td>
					</tr>
					<tr>
						<td>Mercredi</td>
						<td>9h45 - 12h15 14h30 - 19h00</td>
					</tr>
					<tr>
						<td>Jeudi</td>
						<td>17h00 - 19h00</td>
					</tr>
					<tr>
						<td>Vendredi</td>
						<td>9h45 - 12h15 14h30 - 19h00 </td>
					</tr>
					<tr>
						<td>Samedi</td>
						<td>9h45 - 12h15 14h30 - 19h00 </td>
					</tr>
					<tr>
						<td>Dimanche &nbsp;</td>
						<td>9h45 - 12h15 14h30 - 19h00 </td>
					</tr>
				</table>

			</div>
		</div>
		<div class="col-md-4">
			<div id="googleMap"  style="height:800px;width:100%; display : block;"></div>

			<!-- Add Google Maps -->
			<script src="http://maps.googleapis.com/maps/api/js"></script>
			<script>
				var myCenter = new google.maps.LatLng(47.088354, -1.282518);

          function initialize() {
            var mapProp = {
              center: myCenter,
              zoom: 12,
              scrollwheel: true,
              draggable: true,
              mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);

            var marker = new google.maps.Marker({
              position: myCenter,
            });

            marker.setMap(map);
          }

          google.maps.event.addDomListener(window, 'load', initialize);
			</script>
		</div>
		<div class="col-md-2"></div>

	</div>
</div>


<script>
	$(document).ready(function() {
      // Add scrollspy to <body>
      $('body').scrollspy({
        target: ".navbar",
        offset: 100
      });

      var smoothAnim = function(event) {
        // Prevent default anchor click behavior
        event.preventDefault();
        // Store hash
        var hash = this.hash;
        // Using jQuery's animate() method to add smooth page scroll
        // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
        $('html, body').animate({
          scrollTop: $(hash).offset().top
        }, 800, function() {
          // Add hash (#) to URL when done scrolling (default click behavior)
          window.location.hash = hash;
        });
      };

      // Add smooth scrolling on all links inside the navbar
      $("#myNavbar a.scroller").on('click', smoothAnim);
      $("#toTop").on('click', smoothAnim);
    });

</script>