<?php $this->layout('layout/default'); ?>

<h1 class="text-center" id="impression">&nbsp;</h1>
<h1 class="text-center text-capitalize" id="impression">Galerie <?=$gal?></h1>
<div class="container-fluid" ng-app="fp">
	<div class="row">
		<div class="col-md-3"></div>

		<div class="col-md-6 text-center">
			<a href="#" onclick="window.history.back()">
				<h3> Retour </h3> </a>

		</div>

		<div class="col-md-3"></div>
	</div>
	<div class="row">
		<div class="col-md-1"></div>

		<div class="col-md-10 text-center">

			<?php foreach($list as $photo) :?>
			<img ng-controller="photo" ng-click="click()" class="img-thumbnail" name="<?=$photo?>" src="getPhoto.php?p=<?=$photo?>" style="width:300px;">
			<?php endforeach ?>

		</div>

		<div class="col-md-1"></div>
	</div>
</div>

<div class="container text-center">

	<!-- Modal -->
	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog modal-lg" style="margin-top:20px">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>

					<h4 class="modal-title">
					Voir photo
					</h4>
					
					<h5  class="modal-title">
					<span class="rotate glyphicon glyphicon-share-alt " onclick="right()"></span>
					Rotation
					<span class="rotate glyphicon glyphicon-share-alt mirror-y" onclick="left()"></span>
					</h5>
					<script>
							var orient =0;
	
					left = function() {
						orient -= 90;
						$("#bigphoto").rotate({ animateTo : orient, duration : 200});
						return false;
					}
					
					right = function() {
						orient += 90;
						$("#bigphoto").rotate({ animateTo : orient, duration: 200});
						return false;
					}
					</script>
				</div>
				<div class="modal-body">
					<div class="row"  style="overflow : hidden">
						<img style="margin: auto; margin-top: 30px; margin-bottom: 30px; max-height: 70vh;" class="img-responsive" src="" id="bigphoto">
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
				</div>
			</div>
		</div>
	</div>

</div>


<script>

	
	var app = angular.module('fp', []);
		app.controller('photo', function($scope, $window , $http, $location, $element) {
			$scope.click = function(){
				$("#bigphoto").attr('src',$element.attr('src'));
				$("#myModal").modal("show");
				console.log($element.attr('src'));
			}
			
		});

</script>