<?php
session_start();

require_once __DIR__."/vendor/autoload.php";
require_once __DIR__."/vharabar/Galery.php";

require_once __DIR__."/scripts/local.php";


$templates = new League\Plates\Engine(__DIR__.'/templates/');

$g = new Galery();

$dir = $g->publicDirTree();
$list = array();

foreach ($dir as  $value) {
	preg_match("/.*\.(.+)/",$value,$match);
	$phot = $g->getPublicGaleryPhotoList($value);
	$list[$match[1]] =[$value, array_pop($phot)];
}


	
$templates->addData([
	'gals' => $list,
	'var' => getNavText()
	]);

echo $templates->render("galery");


?>