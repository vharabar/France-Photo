<?php
ini_set('memory_limit','2048M');
error_reporting(0);
//Guidance:
//all client related photos are stored in /photo/<galery name>
//all thumbnails of clinet photos with watermark are stored in /thumbnail/
//all miscelanious images (cabine photo, adverts) are stored in /images
//all locaton are relative to site root

require_once __DIR__."/../vendor/abeautifulsite/SimpleImage.php";
require_once __DIR__."/DB.php";


class Galery{
    
    public $photo_dir;
    public $thumb_dir;
    public $image_dir;
    
    public function __construct(){
        
        $this->photo_dir= __DIR__."/../photos/";
        $this->thumb_dir= __DIR__."/../thumbnails/";
        $this->image_dir= __DIR__."/../images/";
        $this->db = new DB();
    }
    
    public function scramble($string){
        return substr(md5($string),0,10);
    }
    
    public function trimmDir($t){
        foreach ($t as $k => $v) {
            if(preg_match("/^\.{1,2}$/",$v)!=0)
            unset($t[$k]);
        }
        
        return $t;
    }
    
    public function photoDirTree(){
        $dir;
        $t = $this->trimmDir(scandir(__DIR__."/../photos/"));
        foreach ($t as $k => $v) {
            $dir[$v] = $this->trimmDir(scandir(__DIR__."/../photos/".$v));
        }
        return $dir;
    }
    
    public function publicDirTree(){
        $dir = $this->trimmDir(scandir(__DIR__."/../public/"));
        return $dir;
    }
    
    public function imagesDirTree(){
        $dir = $this->trimmDir(scandir(__DIR__."/../images/"));
        return $dir;
    }
    
    public function createThumbnail($imgLoc,$watermark=false, $watermark_image=null){
        $size = filesize($imgLoc);
        // if($size > 8388608)
        // return  new SimpleImage(__DIR__."/../images/logo.png");
        
        $img = "";
        try{
            $img = new SimpleImage($imgLoc);
        }catch(Exception $e){
            // print "$e\n";
            return null;
        }
        $img->fit_to_width(1024)->best_fit(1024, 768);
        
        if($watermark!=false){
            $wat = $watermark_image == null? new SimpleImage(__DIR__."/../images/logo.png") : new SimpleImage($watermark_image) ;
            // Get the current width
            $width = $img->get_width();
            
            // Get the current height
            $height = $img->get_height();
            $wat->best_fit($width, $height);
            $img->auto_orient()->overlay($wat,'bottom right', .7 ,0 ,0);
        }
        return $img;
    }
    
    public function genarateKey(){
        $key='';
        $res=null;
        do{
            $key = substr(md5(uniqid()),0,10);
            $res = $this->db->getGaleryByKey($key);
            $size = count($res);
        }while($size!=0);
        
        return $key;
    }
	
	public function getAllGaleries($acc_id,$admin = false){
		if($admin) return $this->db->getGaleries();
		
		$result = array();
        
        $res = $this->db->getGaleryAccess($acc_id);
        if($res==null or count($res)==0){
            return null;
        }
        
        foreach ($res as  $value) {
            $res2 = $this->db->getGaleryById($value['gal_id']);
            $result[]=$res2[0];
        }
        
        return $result;
	}
    
    public function updateUserGaleries(){
        //delete old galeries
        $gallist = $this->db->getGaleries();
        $galloc = array();
        foreach ($gallist as $key ) {
            if(!is_dir(__DIR__."/../photos/".$key['location'])){
				$this->db->removeEveryGaleryAccess($key['id']);
				$a = $this->db->error();
				$a = $this->db->log();
                $this->db->removeGalery($key['id']);
				$a = $this->db->error();
				$a = $this->db->log();
            }
            else{
                $galloc[]=$key['location'];
            }
        }
        
        //add new galeries
        $dir = $this->photoDirTree();
        $gals = array_keys($dir);
        
        $inter = array_diff($gals,$galloc);
        
        foreach ($inter as $value) {
            if(preg_match('/^(.+)\..*/',$value)==0){
                $key = $this->genarateKey();
                $location = $key.".".$value;
                rename(__DIR__."/../photos/".$value, __DIR__."/../photos/".$location);
                //print_r(array(['name' => $value ,'location' =>$location, 'key' => $key]));
                $id = $this->db->addGalery($value,$location,$key);
            }else{
                $match='';
                preg_match('/^(.+)\.(.*)/',$value,$match);
                $id = $this->db->addGalery($match[2],$match[0],$match[1]);
            }
        }
        $this->updateUserGaleriesThumbnails();
    }
    
    
    
    public function updateUserGaleriesThumbnails(){
        $res = $this->db->getGaleries();
        
        foreach( $res as $key => $value){
            $id = $value['id'];
            $location = $value['location'];
            $dir = $this->trimmDir(scandir ( __DIR__."/../photos/".$location));
            foreach ($dir as $key2 => $value2) {
                $value3 = $this->scramble(preg_replace("/\..+$/","",$value2));
                $thumbLoc = __DIR__."/../thumbnails/".$id.".".$value3.".jpg";
                if(!file_exists($thumbLoc)){
                    try{
                        $img = $this->createThumbnail(__DIR__."/../photos/".$location."/".$value2,true);
                        if($img == null)
                        continue;
                        $img->save($thumbLoc);
                        unset($img);
                    }catch(Exception $e){
                        // print "$e\n";
                    }
                }
            }
        }
    }
    
    public function updatePublicGaleries(){
        $pubs = $this->publicDirTree();
        foreach ($pubs as $folder) {
            $dir = $this->trimmDir(scandir(__DIR__."/../public/".$folder));
            foreach ($dir as $value) {
                $value2 = preg_replace("/\..+$/","",$value);
                $value2 = $this->scramble($value2);
                $tmbloc = __DIR__."/../thumbnails/p.$folder.$value2.jpg";
                if(!file_exists($tmbloc)){
                    try{
                        $imgLoc = __DIR__."/../public/$folder/$value";
                        $img = $this->createThumbnail($imgLoc,true);
                        if($img != null)
                        $img->save($tmbloc);
                        unset($img);
                    }catch(Exception $e){
                        // print "$e\n";
                    }
                }
            }
        }
    }
    
    public function cleanThumbnails(){
        $res = $this->db->getGaleries();
        $list = array();
        
        foreach( $res as $key => $value){
            $id = $value['id'];
            $location = $value['location'];
            $dir = $this->trimmDir(scandir ( __DIR__."/../photos/".$location));
            foreach ($dir as $key2 => $value2) {
                $value3 = $this->scramble(preg_replace("/\..+$/","",$value2));
                $thumbLoc = $id.".".$value3.".jpg";
                $list[] = $thumbLoc;
            }
        }
        
        $pubs = $this->publicDirTree();
        foreach ($pubs as $folder) {
            $dir = $this->trimmDir(scandir(__DIR__."/../public/".$folder));
            foreach ($dir as $value) {
                $value2 = $this->scramble(preg_replace("/\..+$/","",$value));
                $thumbLoc = "p.$folder.$value2.jpg";
                $list[] = $thumbLoc;
            }
        }
        
        $thumbs = $this->trimmDir(scandir(__DIR__."/../thumbnails/"));
        
        $diff = array_diff($thumbs,$list);
        
        foreach($diff as $file){
            $location = __DIR__."/../thumbnails/$file";
            if(file_exists($location))
            unlink($location);
        }
    }
    
    public function addAccess($acc_id, $key){
        $result['code']=-1;
        $result['message']="Erreur serveur";
        
        $res = $this->db->getGaleryByKey($key);
        if($res==null or count($res)==0){
            $result['code']=-1;
            $result['message']="Clé invalide";
            return $result;
        }
        
        $gal_id = $res[0]['id'];
        $this->db->addGaleryAccess($acc_id, $gal_id);
        
        $result['code']=0;
        $result['message']="Succes";
        return $result;
    }
    
    public function getUsersGaleries($acc_id){
        $result = array();
        
        $res = $this->db->getGaleryAccess($acc_id);
        if($res==null or count($res)==0){
            return null;
        }
        
        foreach ($res as  $value) {
            $res2 = $this->db->getGaleryById($value['gal_id']);
            $result[$res2[0]['id']] = [$res2[0]['location'],$res2[0]['name']];
        }
        
        return $result;
    }
    
    
    
    public function updateUserGaleryName($acc_id, $gal_id, $name, $admin = false){
        if(!$admin){
            $res = $this->db->getGaleryAccess($acc_id, $gal_id);
            if($res==null or count($res)==0){
                $result['code']=-1;
                $result['message']="Erreur accés galeire";
                return $result;
            }
        }
        
        $this->db->updateGaleryName($gal_id,$name);
        
    }
    
    public function getGalleryPhotoList($acc_id,$gal_id, $admin=false){
        if(!$admin){
            $res = $this->db->getGaleryAccess($acc_id, $gal_id);
            if($res==null or count($res)==0){
                return null;
            }
        }
        
        $res = $this->db->getGaleryById($gal_id);
        
        $location = __DIR__."/../photos/".$res[0]['location'];
        
        $dir = $this->trimmDir(scandir($location));
        
        foreach ($dir as $key => $value) {
            $value = preg_replace("/\..+$/","",$value);
            $dir[$key] = $gal_id.".".$value;
        }
        
        return $dir;
        
    }
    
    public function getPublicGaleries(){
        return $this->publicDirTree();
    }
    
    public function getPublicGaleryPhotoList($gal){
        if(!is_dir(__DIR__."/../public/$gal"))
        return;
        
        $dir = $this->trimmDir(scandir(__DIR__."/../public/$gal"));
        
        foreach($dir as $key => $value){
            // $value2 = $this->scramble(preg_replace("/\.[a-zA-z]+$/","",$value));
            $value2 = preg_replace("/\..+$/","",$value);
            $tmbloc = "p.$gal.$value2";
            $dir[$key] = $tmbloc;
        }
        
        return $dir;
    }
    
    
    public function getPhoto($photo, $acc_id, $admin = false){
        $pres='';
        $count = preg_match("/^(.+)\./",$photo,$pres);
        if($count < 1){
            $img = new SimpleImage(__DIR__."/../images/logo.png");
            $img ->output();
            return;
        }
        $pres2='';
        preg_match("/^(.+\.)(.+)$/",$photo,$pres2);
        
        $pres2[2] = (preg_replace("/\..+$/","",$pres2[2]));
        
        $photo = $pres2[1].$this->scramble($pres2[2]);
        
        if(!file_exists(__DIR__."/../thumbnails/".$photo.".jpg")){
            $img = new SimpleImage(__DIR__."/../images/logo.png");
            $img ->output();
            return;
        }
		
		preg_match("/^(.)\./",$pres[1],$pres);
        
        switch ($pres[1]) {
            case 'p':
                $img = new SimpleImage(__DIR__."/../thumbnails/".$photo.".jpg");
                $img ->output();
                return;
                break;
            
            default:
                if(!$admin){
                    $res = $this->db->getGaleryAccess($acc_id, $pres[1]);
                    if($res==null or count($res)==0){
                        $img = new SimpleImage(__DIR__."/../images/logo.png");
                        $img ->output();
                }
            }
            
				$img = new SimpleImage(__DIR__."/../thumbnails/".$photo.".jpg");
				$img ->output();
				return;
				
				break;
			}
	}
	
	public function estimateConvertionVolume(){
		$res = $this->db->getGaleries();
        $counter = 0;
        foreach( $res as $key => $value){
            $id = $value['id'];
            $location = $value['location'];
            $dir = $this->trimmDir(scandir ( __DIR__."/../photos/".$location));
            foreach ($dir as $key2 => $value2) {
                $value3 = $this->scramble(preg_replace("/\..+$/","",$value2));
                $thumbLoc = __DIR__."/../thumbnails/".$id.".".$value3.".jpg";
                if(!file_exists($thumbLoc)){
                    $counter++;
                }
            }
        }
		
		$pubs = $this->publicDirTree();
        foreach ($pubs as $folder) {
            $dir = $this->trimmDir(scandir(__DIR__."/../public/".$folder));
            foreach ($dir as $value) {
                $value2 = preg_replace("/\..+$/","",$value);
                $value2 = $this->scramble($value2);
                $tmbloc = __DIR__."/../thumbnails/p.$folder.$value2.jpg";
                if(!file_exists($tmbloc)){
                    $counter++;
                }
            }
        }
		
		return $counter == 0 ? "Pas de " : $counter;
	}
}
?>