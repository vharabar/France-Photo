<?php
	
	class Input{
		
		public function getJson(){
			$json =file_get_contents('php://input');
			$obj = json_decode($json,true);
			return $obj;
		}
		
		public function sendJson($obj){
			$res = json_encode($obj);
			echo $res;
		}
	}

?>