<?php

require_once __DIR__."/../vendor/autoload.php";
require_once __DIR__."/Config.php";

class DB{
	public $db;
	
	public function __construct($debug=false){
		$this->db = new  medoo([
			'database_type' => 'mysql',
			'database_name' => 'francephoto',
			'server' => $GLOBALS['cfg']['sql_server'],
			'username' => $GLOBALS['cfg']['sql_user'],
			'password' => $GLOBALS['cfg']['sql_pass'],
			'charset' => 'utf8',
			'port' => 3306,
			'option' => [PDO::ATTR_CASE => PDO::CASE_NATURAL]]);
	}
	
	public function addUser($email, $pw_hash, $name, $token){
		return $this->db->insert('account',['email'=>$email,'pw_hash'=>$pw_hash,'name'=>$name,'token'=>$token]);
	}
	
	public function getUser($data){
		if(is_numeric($data))
			return $this->db->select('account','*',  ['id'=>$data]);
		
		return $this->db->select('account','*',
			['OR'=>['email'=>$data,'token'=>$data]]);
	}
	
	
	public function updateUserPassword($id,$pw_hash){
			return $this->db->update('account',['pw_hash'=>$pw_hash],  ['id'=>$id]);
	}
	
	public function updateUserName($id,$name){
			return $this->db->update('account',['name'=>$name],  ['id'=>$id]);

	}
	
	public function updateUserEmail($id,$email){
			return $this->db->update('account',['email'=>$email],  ['id'=>$id]);

	}
	
	public function addAdmin($id){
		return $this->db->insert('admins',['acc_id'=>$id]);
	}
	
	public function getAdmin($id){
		return $this->db->select('admins','*',['acc_id'=>$id]);
	}
	
	public function addGalery($name, $location, $key){
		return $this->db->insert('galery',['name'=>$name, 'location' =>$location, 'gal_key'=>$key]);
	}
	
	public function getGaleryById($id){
			return $this->db->select('galery','*',['id'=>$id]);
	}
	
	public function getGaleryByKey($key){
		return $this->db->select('galery','*',['gal_key'=>$key]);
	}
	
	public function getGaleries(){
			return $this->db->select('galery','*',['ORDER' => "id DESC"]);
	}
	
	public function updateGaleryName($gal_id, $name){
		$this->db->update('galery', ['name' => $name], ['id' => $gal_id]);
	}
	
	public function removeGalery($data){
		if(is_numeric($data))
			return $this->db->delete('galery',['id'=>$data]);
		
		return $this->db->delete('galery',['name'=>$data]);
		
	}
	
	public function addGaleryAccess($acc_id,$gal_id){
		return $this->db->insert('galery_access',
			['acc_id'=>$acc_id,'gal_id'=>$gal_id]);
	}
	
	public function getGaleryAccess($acc_id,$gal_id = null){
		if($gal_id != null)
		return $this->db->select('galery_access','*',
			['AND'=>['acc_id'=>$acc_id,'gal_id'=>$gal_id]]);
		else
		return $this->db->select('galery_access','*',
			['acc_id'=>$acc_id]);
	}
	
	public function removeGaleryAccess($acc_id,$gal_id = null){
		
		return $this->db->delete('galery_access',
			['AND'=>['acc_id'=>$acc_id,'gal_id'=>$gal_id]]);
	}
	
	public function removeEveryGaleryAccess($gal_id){
		
		return $this->db->delete('galery_access',
			['gal_id'=>$gal_id]);
	}
	
	public function addPhoto($name,$gal_id,$disk_loc,$thumb_loc){
		return $this->db->insert('photo',
			['gal_id'=>$gal_id,'disk_loc'=>$disk_loc,
			'name'=>$name,'thumb_loc'=>$thumb_loc]);
	}
	
	public function getPhoto($data){
		if(is_numeric($data))
			return $this->db->select('photo','*',['id'=>$data]);
			
		return $this->db->select('photo','*',['OR'=>
			['name'=>$data,'disk_location'=>$data]]);
	}
	
	public function removePhoto($data){
		if(is_numeric($data))
			return $this->db->delete('photo',['id'=>$data]);
				
		return $this->db->delete('photo',['disk_location'=>$data]);
	}
	
	public function addOrder($acc_id,$date,$text){
		return $this->db->insert('cl_order',['acc_id'=>$acc_id,'date'=>$date,'details'=>$text]);
	}

	public function getAllOrders($off = 0, $lim = 10){
			return $this->db->select('cl_order','*',['LIMIT' => [$off, $lim], 'ORDER' => "id DESC"]);
	}
	
	public function getOrderById($data){
			return $this->db->select('cl_order','*',['id'=>$data]);
	}
	
	public function getOrderByAcc($data){
			return $this->db->select('cl_order','*',['acc_id'=>$data]);
	}
	
	public function updateOrder($id,$text){
			return $this->db->update('cl_order',['details'=>$text],['acc_id'=>$id]);
	}
	
	public function addOrderItem($ord_id,$pho_id,$opt){
		return $this->db->insert('cl_order_entries',
			['ord_id'=>$ord_id,'pho_od'=>$pho_id,'option'=>$opt]);
	}
	
	public function getOrderItem($data){
		return $this->db->select('cl_order_entries','*',[
			'id'=>$data]);
	}
	
	public function getOrderItemList($data){
		return $this->db->select('cl_order_entries','*',[
			'ord_id'=>$data]);
	}
	
	public function updateToken($id, $token){
		return $this->db->update('account',['tockem'=>$token],['id'=>$id]);
	}
	
	public function error(){
		return $this->db->error();
	}
	
	public function log(){
		return $this->db->log();
	}
}


?>