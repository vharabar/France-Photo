<?php
error_reporting(0);
session_start();
require_once __DIR__."/Cart.php";
require_once __DIR__."/User.php";
require_once __DIR__."/Galery.php";
require_once __DIR__."/DB.php";
require_once __DIR__."/Email.php";

class Order{
    
    public function __construct(){
        $this->db = new DB();
        $this->u = new User();
        $this->g = new Galery();
        $this->c = new Cart();
		$this->em = new Email();
    }
    
    public function processOrder($info){
        $r['code']=-100;
        $r['message']="Erreur serveur";
        
        $user = $_SESSION['user'];
        $cart = $this->c->getCart();
        
        if(count($cart)<1){
            $r['code']=-1;
            $r['message']="Impossible de passer une commande vide";
            return $r;
        }
        
        $gals = $this->g->getUsersGaleries($user['id']);
        
        $order['info'] = $info;
        
        foreach($cart as $value){
            $match = '';
            preg_match('/^(.+)\.(.+)$/',$value[0],$match);
            $gal_id= $match[1];
            $order[$gals[$gal_id][0]][]= [$match[2],$value[1],$value[2]];
        }
        
        $result = json_encode($order);
        $id = $user['id'];
        $date = date("Y-m-d");
        
        $ordid = $this->db->addOrder($id,$date,$result);
        $this->em->sendEmail($ordid);
        $r['code']=0;
        $r['message']="la commande a été passé";
        return $r;
    }
    
    
    
	
}

?>